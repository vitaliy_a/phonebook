# -*- coding: utf-8 -*-
import json 

class Phonebook:
    def __init__(self):
        self.filename = 'data.json'
        #get data from existing file or set data as empty list
        with open(self.filename, 'r') as file:
            try:
                self.phonebook = json.load(file)
            except Exception, e:
                self.phonebook = []
        #run menu
        self.menu()
        
    def menu(self):
        menu_text = '''
1 - show all people
2 - add person
3 - find person by name
4 - find person by phone number
5 - delete person by id
6 - save changes and exit
Enter number from 1 to 6 to perform desired action:
'''
        actions = {
            '1' : self.show,
            '2' : self.add_person,
            '3' : self.find_by_name,
            '4' : self.find_by_number,
            '5' : self.delete_person,
            '6' : self.save
        }
        keep_asking = True
        
        while keep_asking:
            choise = raw_input(menu_text)
            if choise in actions.keys():
                actions[choise]()
                if choise == '6':
                    keep_asking = False
            else:
                print 'Enter valid number to perform desired action:\n'

    def add_person(self):
        #get person data from command line
        person = {}
        person['first_name'] = raw_input('Enter first name:\n')
        person['last_name'] = raw_input('Enter last name:\n')
        person['phone_numbers'] = self.get_phone_numbers()
        #set inctemented id
        person['id'] = self.generate_id()
        #save person in phonebook
        self.phonebook.append(person)
    
    def show(self, book = None):
        if book is None:
            book = self.phonebook

        #table header
        print ' Id | Name ' + ' ' * 20 + ' | Phone number' 
        print '-----------' + '-' * 20 + '---' + '-' * 15

        #table body    
        for person in book:
            formated_string = '{:^3}'.format(person['id']) + ' | '
            formated_string += u'{:<25}'.format(person['first_name'] + ' ' + person['last_name']) + ' | '
            formated_string += self.format_numbers(person['phone_numbers'])
            print formated_string
              
    def delete_person(self):
        id = int(raw_input('Enter person id:\n'))
        ids = [person.get('id') for person in self.phonebook]
        if id in ids:
            self.phonebook = [person for person in self.phonebook if not (id == person.get('id'))]
            print 'Person was successfully removed'
        else:
            print 'Person not found'

    def find_by_name(self):
        name = raw_input('Enter person name:\n').lower().decode('utf-8')
        results = []
        for person in self.phonebook:
            if (name in person.get('first_name', '').lower()) or (name in person.get('last_name', '').lower()):
                results.append(person)
        if len(results) > 0:
            self.show(results)
        else:
            print 'Nothing found'

    def find_by_number(self):
        phone_number = raw_input('Enter phone number:\n')
        results = []
        for person in self.phonebook:
            if phone_number in person.get('phone_numbers'):
                results.append(person)
        if len(results) > 0:
            self.show(results)
        else:
            print 'Nothing found'

    def save(self):
        with open(self.filename, 'w') as file:
            file.write(json.dumps(self.phonebook, indent = 4))            

    #helpers

    def generate_id(self):
        ids = [person.get('id', 0) for person in self.phonebook]
        ids.sort()
        try:
            id = ids[-1] + 1
        except Exception, e:
            id = 1
        return id

    def get_phone_numbers(self):
        phone_numbers = []
        phone_number = True;
        while phone_number:
            phone_number = raw_input('Enter phone number or leave blank if you want to end:\n')
            if phone_number:
                phone_numbers.append(phone_number)
        return phone_numbers
    
    def format_numbers(self, numbers):
        result = ''
        if len(numbers) > 0:
            result = u'{:<15}'.format(numbers[0]) + '\n'
            for number in numbers[1:]:
                result += ' ' * 34 + u'{:<15}'.format(number) + '\n'
            result = result.rstrip('\n')
        return result

Phonebook()